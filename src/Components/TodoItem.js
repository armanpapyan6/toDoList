import React from 'react'

// import Button from "@mui/material/Button";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from "@mui/material/TextField";
// import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';

class TodoItem extends React.Component{
   state={
        removeOpen: false,
        editOpen:false,
        itemEditValue : this.props.data.value,
        checked: false

   }

   toRemove = ()=>{
       this.props.toDoRemoveClick(this.props.data.id)
       this.handleClose()
   }
   questionRemove = ()=>{
    this.setState({removeOpen: true})
   }

   questionEdit = ()=>{
       this.setState({editOpen:true})
   }
   onEditItem = () =>{
       this.props.editListItem(this.props.data.id, this.state.itemEditValue)
       this.handleClose1()
   }

   handleClose=()=>{
        this.setState({removeOpen: false})
    }
    handleClose1=()=>{
        this.setState({itemEditValue: this.props.data.value, editOpen:false})
    }
    onChangeEditValue = (e)=>{
       this.setState({itemEditValue: e.target.value})
        console.log(this.state.itemEditValue)
    }
    handleChangeCHeckbox = ()=>{
        this.setState({checked: !this.state.checked})
    }
    onKeyDown1 = (e)=>{
        if(e.key === 'Enter'){
            this.onEditItem()
        }

    }


    render(){
        return  (
            <li className='listItem'>
                <Checkbox
                    checked={false}
                    onChange={true}
                    inputProps={{ 'aria-label': 'controlled' }}
                />
                <span>{this.props.data.value}</span>
                <Dialog
                        open={this.state.removeOpen}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        >
                        <DialogTitle id="alert-dialog-title">
                        {"Remove"}
                        </DialogTitle>
                        <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                           Do you want remove  {this.props.data?.value} element ?
                        </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose}>No</Button>
                            <Button onClick={this.toRemove} autoFocus>
                                Yes
                            </Button>
                        </DialogActions>
                </Dialog>
                <Dialog
                        open={this.state.editOpen}
                        onClose={this.handleClose1}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        >
                        <DialogTitle id="alert-dialog-title">
                        {"Remove"}
                        </DialogTitle>
                        <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                          <p>Do you want edit  this element ?</p>
                            <div>
                                <TextField
                                label="Add new Message..."
                                id="outlined-size-small"
                                defaultValue="Small"
                                size="small"
                                value={this.state?.itemEditValue}
                                onChange={this.onChangeEditValue}
                                // onKeyDown={this.onKeyDown}
                            /></div>
                        </DialogContentText>
                        </DialogContent>
                        <DialogActions>

                            <Button onClick={this.handleClose1}>No</Button>
                            <Button onClick={this.onEditItem} autoFocus>Yes</Button>
                        </DialogActions>
                </Dialog>
                <Button disabled={false} onClick={this.questionEdit} variant="text">Edit</Button>
                <Button disabled={false} onClick={this.questionRemove} variant="text">Remove</Button>
            </li>
                )
            }
}

export default TodoItem