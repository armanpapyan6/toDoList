import React from 'react';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';


class AddInput extends React.Component{
    render() {
        return(
            <>
            <div className='flexInput'>
                <TextField
                    label="Add new Message..."
                    id="outlined-size-small"
                    defaultValue="Small"
                    size="small"
                    value={this.props.inputValue}
                    onChange={this.props.onChange}
                    onKeyDown={this.props.onKeyDown}
                />
                <Button disabled={false} onClick={this.props.onclick} variant="contained">Contained</Button>
            </div>
                
                </>
        )
    }
}
export default AddInput;