import './App.css';
import React from 'react'
import Button from '@mui/material/Button';
import AddInput from "./Components/AddInput";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import TodoItem from "./Components/TodoItem";
import {uuid4} from 'uuid4'

class App extends  React.Component{
state={
   list: [],
    inputValue:'',

}
inputChange =(e)=>{
    this.setState({inputValue:e.target.value})
    console.log(e.target.value)
}
addText = ()=>{
  if(this.state.inputValue !== ''){
      this.setState(
          {
              list:this.state.list.concat({
                  id:uuid4(),
                  value: this.state.inputValue}),
              inputValue:''
          })
  }

    // console.log(uuid4())
}
editListItem =(id,text)=>{
    this.state.list.forEach((elem, index, masiv)=>{
        if(elem.id == id){
            console.log(id)
            console.log(index)
            masiv[index].value = text
            this.setState({list:masiv})

        }else{
            console.log(' notwe')
        }

    })
}
toDoRemoveClick = ( id)=>{
    this.state.list.forEach((elem, index,)=>{
        if(elem.id === id){
            this.state.list.splice(index,1)
            this.setState({list:this.state.list})
        }else{
            console.log(' notwe')
        }

    })
}
    onKeyDown =(event)=>{
    if(event.key === 'Enter'){
      this.addText()
    }}

  render() {
      return (
          <div  className='App'>
              <p>To Do List</p>
              <div>
                  <div className='inputBlock'>
                      <AddInput   onKeyDown={this.onKeyDown} onclick={this.addText} onChange={this.inputChange} inputValue={this.state.inputValue}/>
                  </div>
              </div>
              <ul>
                  {
                      this.state.list.map(elem=>{
                          return (<TodoItem toDoRemoveClick={this.toDoRemoveClick} editListItem={this.editListItem} key={elem.id}  data={elem}/>)
                      })
                  }
              </ul>
          </div>
      )
  }
}

export default  App